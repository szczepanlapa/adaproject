
with Glib;                use Glib;
with Glib.Properties;     use Glib.Properties;
with Gtk;                 use Gtk;
with Gdk;                 use Gdk;
with Gtk.Container;
with Gdk.Color;           use Gdk.Color;
with Gtk.Box;             use Gtk.Box;
with Gtk.Button;          use Gtk.Button;
with Gtk.Dialog;          use Gtk.Dialog;
with Gtk.Enums;           use Gtk.Enums;
with Gtk.Frame;           use Gtk.Frame;
with Gtk.Image;           use Gtk.Image;
with Gtk.Handlers;        use Gtk.Handlers;
with Gtkada.Handlers;     use Gtkada.Handlers;
with Gtk.Hbutton_Box;     use Gtk.Hbutton_Box;
with Gtk.Label;           use Gtk.Label;
with Gtk.Main;            use Gtk.Main;
with Gtk.Notebook;        use Gtk.Notebook;
with Gtk.Scrolled_Window; use Gtk.Scrolled_Window;
with Gtk.Paned;           use Gtk.Paned;
with Gtk.Text_Buffer;     use Gtk.Text_Buffer;
with Gtk.Text_Iter;       use Gtk.Text_Iter;
with Gtk.Text_Mark;       use Gtk.Text_Mark;
with Gtk.Text_Tag;        use Gtk.Text_Tag;
with Gtk.Text_Tag_Table;  use Gtk.Text_Tag_Table;
with Gtk.Text_View;       use Gtk.Text_View;
with Gtk.Tree_Model;         use Gtk.Tree_Model;
with Gtk.Tree_Selection;     use Gtk.Tree_Selection;
with Gtk.Tree_Store;         use Gtk.Tree_Store;
with Gtk.Tree_View;          use Gtk.Tree_View;
with Gtk.Tree_View_Column;   use Gtk.Tree_View_Column;
with Gtk.Cell_Renderer_Text; use Gtk.Cell_Renderer_Text;
with Gtk.Widget;          use Gtk.Widget;
with Gtk.Window;          use Gtk.Window;
with Pango.Font;          use Pango.Font;
with Create_Pixbuf;
with Common; use Common;
with Pango.Font;          use Pango.Font;
with Create_Spinners;
with Glib.Main;       use Glib.Main;
with Gtk.Image; use Gtk.Image;
with Gtk.GEntry;       use Gtk.GEntry;
with Gtk.Alignment; use Gtk.Alignment;
With Ada.Strings.Bounded; use Ada.Strings.Bounded;

with RollTask;
use RollTask;

with Game_pack;
use Game_pack;


package body gui is


   procedure RunGame (widget : access Gtk_Widget_Record'Class);
   procedure FinishGame (widget : access Gtk_Widget_Record'Class);
   procedure AddCash (widget : access Gtk_Widget_Record'Class);

   package Window_Cb is new Handlers.Callback (Gtk_Widget_Record);
   package Return_Window_Cb is new Handlers.Return_Callback
     (Gtk_Widget_Record, Boolean);

   procedure Exit_Main (Object : access Gtk_Widget_Record'Class);
   --  Callbacks when the main window is killed
   function Delete_Event
     (Object : access Gtk_Widget_Record'Class) return Boolean;

   --  Timer for pulsing activity of one of our spinners.
   package Time_Cb is new Glib.Main.Generic_Sources (Gtk_Label_Record);

   --  Function passed to Time_Cb.Timeout_Add, to be invoked periodically.
   function Timer_Timeout(Label : Gtk_Label_Record) return Boolean;

   --  A handle referencing our timeout
   Timer : G_Source_Id := No_Source_Id;

   game_state : GameState_t := STOPPED;
   CashEntry : Gtk_Entry;


   procedure RunGame (widget : access Gtk_Widget_Record'Class) is
      Actual_resources: Integer;
   begin
      Game.GetResources(Actual_resources);
      GetGameState(game_state);
      if game_state /= RUNNING then
         if Actual_resources >= 20 then
            Set_Label(testLabel'Access, "Pociągnięto za Łapę");
            Rand;
            Game.DoRoll;
         else
            Set_Label(testLabel'Access, "Brak Srodkow");
         end if;
      end if;
   end RunGame;


   procedure FinishGame (widget : access Gtk_Widget_Record'Class) is
      receivedCash : Integer;
   begin
      Game.ReceiveCash(receivedCash);
   end FinishGame;

   procedure AddCash(widget : access Gtk_Widget_Record'Class) is
      cash : Integer;
   begin
      cash := Integer'Value(Get_Text(CashEntry));
      if cash > 0 and then cash < 1000000 then
         Game.AddCash(dollars => cash);
      end if;
      exception
     when Constraint_Error =>
        Set_Text (CashEntry, "Nieprawidłowa wartość");
   end AddCash;

   -------------
   -- Gtk_New --
   -------------

   procedure Gtk_New (Win : out Main_Window) is
   begin
      Win := new Main_Window_Record;
      gui.Initialize (Win);
   end Gtk_New;
   -----------------
   --  Exit_Main  --
   -----------------

   procedure Exit_Main (Object : access Gtk_Widget_Record'Class) is
   begin
      QuitGame;
      Destroy (Object);
      Gtk.Main.Main_Quit;
   end Exit_Main;

   ------------------
   -- Delete_Event --
   ------------------

   function Delete_Event
     (Object : access Gtk_Widget_Record'Class) return Boolean
   is
      pragma Unreferenced (Object);
   begin
      --  We could return True to force the user to kill the window through the
      --  "Quit" button, as opposed to the icon in the title bar.
      return False;
   end Delete_Event;


   function Timer_Timeout (Label : Gtk_Label_Record) return Boolean is
      sign : Signs;
      resources : Integer;
      --Image : Gtk_Image_Record;
   begin
      --        Set_Label(testLabel'Access, "Timer");
      GetGameState(game_state);
      case game_state is
         when STOPPED    =>  Set_Label(testLabel'Access, "Game stopped");
         when PAUSED     =>  Set_Label(testLabel'Access, "Game Paused");
         when RUNNING    =>  Set_Label(testLabel'Access, "Game running");
         when others => null;
      end case;

      for i in 0..2 loop
         GetRollValue(i, sign);
         Gtk.Image.Destroy(rollImage(i)'Access);
         case sign is
            when sev    => Initialize (rollImage(i)'Access, Filename => "imgs/sev.jpg");--Set_Label(rollLabel(i)'Access, " 7 ");
            when gun    => Initialize (rollImage(i)'Access, Filename => "imgs/gun.jpg");
            when bar    => Initialize (rollImage(i)'Access, Filename => "imgs/bar.jpg");
            when pie    => Initialize (rollImage(i)'Access, Filename => "imgs/pie.jpg");
            when others => -- error has already been signaled to user
               null;
         end case;
         Gtk.Frame.Add (rollFrame(i), rollImage(i)'Access);
         Show_All (rollFrame(i));
      end loop;

      CheckRollsState(game_state);
      CheckRoundEnd;
      Game.GetResources(resources);
      Set_Label(cashLabel'Access, "CASH:" & resources'Img&"$");

      return True;
   end Timer_Timeout;

   procedure Initialize (Win : access Main_Window_Record'Class) is
      Label    : Gtk.Label.Gtk_Label;
      Vbox     : Gtk.Box.Gtk_Box;
      TopHbox, MiddleHbox, CashHBox   : Gtk_Hbox;
      Button   : Gtk.Button.Gtk_Button;
      Bbox    : Gtk.Hbutton_Box.Gtk_Hbutton_Box;
      LeftAnimFrame, RightAnimFrame, Frame2 : Gtk.Frame.Gtk_Frame;
      Resources : Integer :=0;

      Align  : Gtk_Alignment;

   begin

      InitializeGame;
      Gtk.Window.Initialize (Win, Gtk.Enums.Window_Toplevel);
      Set_Default_Size (Win, 300, 400);
      Window_Cb.Connect (Win, "destroy",
                         Window_Cb.To_Marshaller (Exit_Main'Access));
      Return_Window_Cb.Connect
        (Win, "delete_event",
         Return_Window_Cb.To_Marshaller (Delete_Event'Access));

      --  The global box
      Gtk_New_Vbox (Vbox, Homogeneous => False, Spacing => 0);
      Add (Win, Vbox);


      Gtk_New_Hbox(TopHbox);
      Vbox.Add(TopHbox);

      Gtk_New (LeftAnimFrame);
      Gtk_New (RightAnimFrame);
      --  Label
      Gtk_New (Label, "   Rozbij Bank!!!   ");
      Override_Font (Label, From_String ("Sans Bold 18"));
      Pack_Start (TopHbox, LeftAnimFrame, Expand => False, Fill => False, Padding => 10);
      Pack_Start (TopHbox, Label, Expand => False, Fill => False, Padding => 10);
      Pack_Start (TopHbox, RightAnimFrame, Expand => False, Fill => False, Padding => 10);

      Create_Pixbuf.Run_Gif(LeftAnimFrame);
      Create_Pixbuf.Run_Gif(RightAnimFrame);

      Gtk_New_Hbox(MiddleHbox);
      Vbox.Add(MiddleHbox);

      for i in rollFrame'Range loop
         Gtk_New (rollFrame(i));
         MiddleHbox.Add(rollFrame(i));
         Initialize (rollImage(i)'Access, "");
         Gtk.Frame.Add (rollFrame(i), rollImage(i)'Access);
         Show_All (rollFrame(i));
      end loop;

      Gtk_New_Hbox(CashHBox);
      Vbox.Add(CashHBox);


      Gtk.Label.Initialize(cashLabel'access,"CASH:" & Resources'Img&"$");
      CashHBox.Add(cashLabel'access);
      Override_Font (cashLabel'access, From_String ("Consolas Bold 15"));


      Gtk.Label.Initialize(testLabel'access, "");
      Vbox.Add(testLabel'access);

      Gtk_New (Frame2, "Podaj ile pieniędzy chcesz wrzucić (stracić)");
      Gtk_New (Align,
               Xalign => 0.0,
               Yalign => 0.2,
               Xscale => 0.01,
               Yscale => 0.2);

      Gtk_New (CashEntry);
      Set_Text (CashEntry, "1000");

      Add (Align, CashEntry);
      Add (Frame2, Align);
      Pack_Start (Vbox, Frame2);

      Gtk_New (Bbox);
      Set_Layout (Bbox, Buttonbox_Spread);
      Set_Spacing (Bbox, 10);

      Gtk_New (Button, "Dorzuć forsę");
      Pack_Start (Bbox, Button, Expand => True, Fill => False);
      Widget_Handler.Object_Connect
        (Button, "clicked",
         Widget_Handler.To_Marshaller (AddCash'Access), Win);

      Gtk_New (Button, "Ciągnij");
      Pack_Start (Bbox, Button, Expand => True, Fill => False);
      Widget_Handler.Object_Connect
        (Button, "clicked",
         Widget_Handler.To_Marshaller (RunGame'Access), Win);


      Gtk_New (Button, "Odbierz"& ASCII.LF & "wygraną");
      Pack_Start (Bbox, Button, Expand => True, Fill => False);
      Widget_Handler.Object_Connect
        (Button, "clicked",
         Widget_Handler.To_Marshaller (FinishGame'Access), Win);

      Gtk_New (Button, "Zamknij");
      Pack_Start (Bbox, Button, Expand => True, Fill => False);
      Window_Cb.Object_Connect
        (Button, "clicked",
         Window_Cb.To_Marshaller (Exit_Main'Access), Win);

      Pack_End (Vbox, Bbox, Expand => False, Padding => 5);
      --

      Timer := Time_Cb.Timeout_Add
        (100, Timer_Timeout'Access, testLabel);

      --  Display everything
      Show_All (Vbox);

   end Initialize;

end gui;
