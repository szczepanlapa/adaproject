with Ada.Numerics.Discrete_Random;


package body RollTask is
   package Random_sign is new Ada.Numerics.Discrete_Random(Signs);


  task body Task_Roll is
      use Random_sign;
     -- RollState : GameState_t;

      RollIndex, Initial_speed: Integer;
      Gen: Random_sign.Generator;

  begin
    accept Construct( index : in Integer) do
         RollIndex := index;
         RollState(RollIndex) := STOPPED;
         Initial_Speed := (3 - index) * 10;
    end Construct;
      Reset(Gen);
    loop
      select
        accept Random do
          null;
        end Random;
            RollState(RollIndex) := RUNNING;
            for i in 0..Initial_speed loop
               RollValue(RollIndex) := Random(Gen);
               delay 0.05;
            end loop;
            RollState(RollIndex) := PAUSED;
      or
        accept Quit do
          null;
        end Quit;
        exit;
      end select;
    end loop;
  end Task_Roll;
end RollTask;
