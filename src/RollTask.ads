
package RollTask is


   type Signs is (sev, bar, gun, pie);
   type GameState_t is (STOPPED, PAUSED, RUNNING);
   subtype RollSpeed_t is integer range 1..30;
   rollState: array(0..2) of GameState_t;
   rollValue : array(0..2) of Signs;
   pragma Atomic_Components(rollValue);
   pragma Atomic_Components(rollState);
  task type Task_Roll is
      entry Construct(index : in Integer);
      entry Random;
      entry Quit;
  end Task_Roll;
end RollTask;
