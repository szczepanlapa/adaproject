

with RollTask;
use RollTask;

package Game_pack is

   procedure GetGameState (state : out GameState_t);
   procedure SetGameState (state : in GameState_t);
   procedure CheckRollsState (state : in GameState_t);
   procedure SetRollValue (rollNumber : in integer; state : in Signs);
   procedure GetRollValue (rollNumber : in integer; state : out Signs);
   procedure CheckRoundEnd;
   procedure InitializeGame;
   procedure QuitGame;
   procedure Rand;

   protected Game is
      procedure checkWin;
      procedure DoRoll;
      procedure AddCash(sign: Signs);
      procedure AddCash(dollars : Integer);
      procedure ReceiveCash(dollars : out Integer);
      procedure GetResources( res : out Integer);

   private
      Resources: Integer := 0;
   end Game;

private
   lastGameState : GameState_t := STOPPED;
end Game_pack;
