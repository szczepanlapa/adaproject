with Gtk.Window;
with Gtk.Notebook;
with Gtk.Label;        use Gtk.Label;
with Gtk.Frame;        use Gtk.Frame;
with Gtk.Image;        use Gtk.Image;

package gui is

type Main_Window_Record is new Gtk.Window.Gtk_Window_Record with
      record
         Notebook       : Gtk.Notebook.Gtk_Notebook;
--           testLabel : Gtk_Label;
      end record;
   type Main_Window is access all Main_Window_Record'Class;

   procedure Gtk_New (Win : out Main_Window);
   procedure Initialize (Win : access Main_Window_Record'Class);

   testLabel : aliased Gtk_Label_Record;
   rollFrame : array (0..2) of aliased  Gtk_Frame;
   cashLabel : aliased Gtk_Label_Record;
   rollImage : array (0..2) of aliased  Gtk_Image_Record;



end gui;
