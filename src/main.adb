with Gtk.Box;         use Gtk.Box;
with Gtk.Label;       use Gtk.Label;
with Gtk.Widget;      use Gtk.Widget;
with Gtk.Main;
with Gtk.Window;      use Gtk.Window;
with Create_Pixbuf;
with Gtk.Frame;           use Gtk.Frame;
with Gtk.Button;          use Gtk.Button;
with Gtkada.Handlers;     use Gtkada.Handlers;
with Common; use Common;
with Gtk.Main;
with gui;
with Ada.Text_IO;        use Ada.Text_IO;
with Gtkada.Bindings;    use Gtkada.Bindings;
with Gtkada.Style;       use Gtkada.Style;
with Gtk.Enums;          use Gtk.Enums;
with Gtk.Style_Provider; use Gtk.Style_Provider;


procedure Main is
   Win   : gui.Main_Window;
begin
   Gtk.Main.Init;
   gui.Gtk_New (Win);
   Win.Set_Position (Win_Pos_Center);
   gui.Show_All (Win);
   Gtk.Main.Main;
end Main;
