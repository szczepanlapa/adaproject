with Ada.Numerics.Discrete_Random;


package body Game_pack is

   -- Game

   gameState : GameState_t := STOPPED;
   pragma Atomic(gameState);

   rollTasks: array(0..2) of access Task_Roll;

   procedure GetGameState ( state : out GameState_t) is
   begin
      state := gameState;
   end GetGameState;

   procedure SetGameState ( state : in GameState_t) is
   begin
      gameState:= state;
   end SetGameState;

   procedure checkRollsState (state : in GameState_t) is
      val : Boolean;
   begin
      val := (for some K in rollState'Range => rollState(K) = RUNNING);
      if val = True then
         SetGameState(RUNNING);
      else
         SetGameState(PAUSED);
      end if;
   end checkRollsState;

   procedure SetRollValue (rollNumber : in integer; state : in Signs) is
   begin
      null;--rollValue(rollNumber):= state;
   end SetRollValue;

   procedure GetRollValue (rollNumber : in integer; state : out Signs) is
   begin
      state := rollValue(rollNumber);
   end GetRollValue;

   procedure CheckRoundEnd is
      curGameState : GameState_t;
   begin
      GetGameState(curGameState);
      if curGameState = PAUSED and then lastGameState = RUNNING then
         Game.checkWin;
      end if;

      lastGameState := curGameState;
   end CheckRoundEnd;


   procedure InitializeGame is
   begin
      rollTasks(0) := new Task_Roll;
      rollTasks(1) := new Task_Roll;
      rollTasks(2) := new Task_Roll;
      rollTasks(0).Construct(0);
      rollTasks(1).Construct(1);
      rollTasks(2).Construct(2);
   end InitializeGame;

   procedure QuitGame is
   begin
      rollTasks(0).Quit;
      rollTasks(1).Quit;
      rollTasks(2).Quit;
   end QuitGame;

   procedure Rand is
   begin
      rollTasks(0).Random;
      rollTasks(1).Random;
      rollTasks(2).Random;
   end Rand;




   protected body Game is
      procedure setValue(index: Natural; S: Signs) is
      begin
         rollValue(index) := S;
      end setValue;

      -- Funkcja checkWin - Sprawdza czy padla wygrana
      procedure checkWin is
      begin
         if rollValue(0) = rollValue(1) then
            if rollValue(1) = rollValue(2) then
               AddCash(rollValue(0));
            end if;
         end if;
      end checkWin;



      -- Funkcja AddCash - dodaje pieniadze w zaleznosci od wylosowanego znaku
      procedure AddCash(sign: Signs) is
      begin
         case sign is
         when sev    => Resources := Resources + 100;
         when gun    => Resources := Resources + 200;
         when bar    => Resources := Resources + 1000;
         when pie    => Resources := Resources + 50;
         when others => null;
         end case;
      end AddCash;

      procedure AddCash(dollars : Integer) is
      begin
         Resources := Resources + dollars;
      end AddCash;

      procedure ReceiveCash(dollars : out Integer) is
      begin
         dollars := Resources;
         Resources := 0;
      end ReceiveCash;

      procedure GetResources( res : out Integer) is
      begin
         res := Resources;
      end GetResources;

      procedure DoRoll is
      begin
         Resources := Resources - 20;
      end DoRoll;


   end Game;

end Game_pack;
